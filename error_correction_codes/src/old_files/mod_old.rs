
use std::env;
use std::fs;
use sparse_bin_mat::SparseBinMat;

pub fn count_dimension(char_vec : &Vec<char>) -> (i32, i32){

    let mut i = 0;
    let mut j = 0;
    let mut temp = false;
    let mut start_counting_number_of_columns = true;

    for e in char_vec{
        if *e=='['{
            i+=1;
        }
        else if *e==']'{
            start_counting_number_of_columns=false;
            temp=false;
        }else if *e==','{
            temp=false;
        }
        else if (*e!='['||*e!=']')&&!temp&&start_counting_number_of_columns{
            j+=1;
            temp = true;
        }
    }
    i-=1;

    (i,j)
}


pub fn get_vector_of_values(mut matrix_from_file : String) -> Vec<i32>{
    let mut temp_char = String::new();

    matrix_from_file.retain(|c| (c!='['));
    matrix_from_file.retain(|c| (c!=']'));
    matrix_from_file.push('!');

    let matrix_from_file_vec : Vec<char>= matrix_from_file.chars().collect();
    let mut values_vec : Vec<i32> = Vec::new();
    
    for c in matrix_from_file_vec{
        if c=='!'{
            values_vec.push(temp_char.parse().unwrap());
            temp_char.clear();
        }else if c!=','{
            temp_char.push(c);
        }else{
            //println!("value to unwrap -> {temp_char}");
            values_vec.push(temp_char.parse().unwrap());
            temp_char.clear();
        }
    }
    values_vec
}


pub fn get_matrix_from_file(path : &str) -> (String, String){
    let mut content = fs::read_to_string("./src/test.txt")
        .expect("A problem has occured while reading the file, please retry later.");
    content.retain(|c| c !=' ');

    let mut matrix_from_file = String::new();

    for line in content.lines(){
        matrix_from_file+=line;
    }
    (matrix_from_file, content)
}

pub fn test_scaled_matrix(){
    let A = sparse_bin_mat::SparseBinMat::new(number_of_columns: usize, rows: Vec<Vec<usize>>);
}